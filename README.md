# Sistema de Encuestas para Alumnos de  Postgrado (SEAP) #

Seap es un sistema desarrollado para la Universidad del Bio Bio con el objetivo de ser un complemento para la encuesta docente de alumnos de pregrado.

# Prerrequisitos #

Para poder ejecutar la aplicacion se requiere Tomcat8

# Instalacion #

El primer paso de instalacion es clonar el repositorio

```
git clone https://fo-maestro@bitbucket.org/fo-maestro/seap.git
```

Luego se debe crear el archivo de configuracion de hibernate

```
src/main/resources/hibernate.cfg.xml
```

Con la siguiente estructura

```
<?xml version = "1.0" encoding = "utf-8"?>
<!DOCTYPE hibernate-configuration SYSTEM "http://www.hibernate.org/dtd/hibernate-configuration-3.0.dtd">
<hibernate-configuration>
    <session-factory>
        <property name="hibernate.dialect">
            org.hibernate.dialect.MariaDBDialect
        </property>
        
        <property name="hibernate.connection.driver_class">
            org.mariadb.jdbc.Driver
        </property>

        <property name="hibernate.connection.url">
            jdbc:mariadb://localhost:3306/SEAP
        </property>

        <property name="hibernate.connection.username">
            username
        </property>

        <property name="hibernate.connection.password">
            password
        </property>

        <mapping class="cl.group2.model.Persona"/>
        <mapping class="cl.group2.model.Usuario"/>
    </session-factory>
</hibernate-configuration>
```

Tambien se debe crear un archivo de credenciales para el acceso al mantenedor de usuarios y el servicio de mailing del sitio

```
src/main/resources/credentials.cfg
```

Con la siguiente estructura

```
[Log In]
USER=user
PASSWORD=password

[Mail Exchange]
USER=example@gmail.com
PASSWORD=password
```

Se deben remplazar los datos de ejemplos por los datos reales que se requieran, las crendenciales de [Log in] corresponde a las de acceso al mantenedor de usuarios, en el caso de [Mail Exchance] corresponde a un correo tipo gmail para el envio de contraseņas para los usuarios registrados en el sistema

# Autores #

* **Felipe Oyarzun**
* **Sergio Monsalvez**
* **Sebastian Lagos**
* **Facundo Luna**
* **Maximiliano F. Reuca**